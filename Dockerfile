FROM ubuntu:16.04
LABEL \
    "purpose"="salt-master" \
    "maintainer"="SixFoot DevOps <devops@6ft.com>"
# Install the things needed to add the repository to docker.
RUN \
    apt-get update && \
    apt-get install -y wget && \
    wget -O - http://repo.saltstack.com/apt/ubuntu/16.04/amd64/archive/2016.11.3/SALTSTACK-GPG-KEY.pub | apt-key add - ; \
    echo "deb http://repo.saltstack.com/apt/ubuntu/16.04/amd64/archive/2016.11.3 xenial main" > /etc/apt/sources.list.d/saltstack.list && \
    apt-get update && \
    apt-get install python-pip salt-master=2016.11.3+ds-1 salt-minion=2016.11.3+ds-1 salt-common=2016.11.3+ds-1 salt-api=2016.11.3+ds-1 salt-cloud sudo vim curl iputils.ping python-git cron -y && \
    pip install CherryPy==3.2.3 && \
    useradd -G sudo -c "AWS Build User" -p WOdoVqE3k38ME build && \
    echo -en "build ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/build


EXPOSE 4505 4506 8008

COPY ./files/salt/ /etc/salt/
COPY ./files/start-salt.sh /usr/bin/
COPY ./files/id_rsa* /root/.ssh/
ENTRYPOINT /bin/bash /usr/bin/start-salt.sh
#VOLUME ["/var/cache/salt", "/etc/salt/pki"]