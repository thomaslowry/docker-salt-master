# Docker Salt Master
## Purpose
The purpose of this Docker container is to setup a Salt Master, Salt Minion, and the Salt API so that one could use it 
in the AWS ECR Container service. This provides persistent data volumes that keep various items available from instance 
to instance of the container.

## Container Details
### Base Image
The base image of this container is Ubuntu 16.04 LTS as this is what we are using currently in our environment. This 
base image is provided officially by Docker themselves.

### Software Installed
Below is the software that is installed on top of this image:

Software    | Version        | Notes                                                         |
----------- | -------------- | ------------------------------------------------------------- |
salt-master | 2016.11.3+ds+1 |                                                               |
salt-minion | 2016.11.3+ds+1 |                                                               |
salt-common | 2016.11.3+ds+1 |                                                               |
salt-cloud  | 2016.11.3+ds+1 |                                                               |
salt-api    | 2016.11.3+ds+1 |                                                               |
CherryPy    | 3.2.3          | Older version used due to SSL issue with newer version 3.5.0. |

### Users
Users added are as follows:

* build

### Ports
The following ports were exposed within the docker and would also need to be exposed via the host machine as well:
* TCP: 4505
* TCP: 4506
* TCP: 8008

### Volumes
The following volumes are defined to provide data persistence. 

| Volume Path                  | Purpose                                                              |
| ---------------------------- | -------------------------------------------------------------------- |
| /var/cache/salt              | Provides caching information for the Salt Master                     |
| /etc/salt/pki                | Stores the Master and Minion keys that provide secure communication. |
| /etc/salt/cloud.profiles.d   | Stores the AWS profiles that provides container information.         |
| /etc/salt/cloud.providers.d  | Stores the AWS provider information on how to connect to AWS.        |

### Configurations
The configurations for both Salt Master, Minion, Reactor, and API are baked into the image. Changing them would require 
a new image to be built.

### Extras
#### Startup Script
There is a start-salt.sh script which take care of starting the entire Salt stack. This script is executed as soon as 
the container is started.

#### Key Files
Currently the key files have been omitted in this repository until we figure out a way to get this information possibly 
from vault. However here is some information regarding the key files:

| Location | Purpose |
| -------- | ------- |
| /files/salt/salt-cloud.pem | The AWS pem file to use when interacting with new containers spun up by Salt Cloud |
| /files/id_rsa | Used to perform Git pulls from Bitbucket. /

#### Git Repositories
The image, once up and running, will clone the following repositories into the following locations:

| Git Repo | Destination Folder |
| -------- | ------------------ |
| [salt-master-cloud](https://bitbucket.org/thomaslowry/salt-master-cloud) | /etc/salt/ |
| [salt-master-state-pillar-files](https://bitbucket.org/thomaslowry/salt-master-state-pillar-files) | /srv/ |

### Building The Image
To build the image, please execute the following command *within* the directory where the Dockerfile is contained. 

`docker build -t salt-master:latest .`

## Running The Container
To be filled out.

# Todo
1. Build a docker file for local testing.
2. Find out exactly what profile definitions need to be made.