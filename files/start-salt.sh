#!/bin/bash
# PREP FOR PULLING FROM GIT
chown root.root /root/.ssh/id_rsa*
chmod 0600 /root/.ssh/id_rsa*
echo -e "Host bitbucket.org\n\tHostname bitbucket.org\n\tUser root\n\tStrictHostKeyChecking no\n\tIdentityFile /root/.ssh/id_rsa\n\tIdentitiesOnly yes\n\tUserKnownHostsFile=/dev/null\n" >> /root/.ssh/config

# CLONE GIT REPO WITH SALT STUFF
git clone git@bitbucket.org:thomaslowry/salt-master-cloud.git /etc/salt/

# SALT CLOUD
chown root.root /etc/salt/salt-cloud.pem
chmod 0400 /etc/salt/salt-cloud.pem

service salt-master start 
service salt-minion start

# Accept Salt Key for Salt-Master.
echo " * Waiting for Salt key to show up."
until [[ $(salt-key -l unac|egrep "^$(hostname)$" -c) -eq 1 ]];do
    sleep 1;
done
echo -en " * Accepting $(hostname)'s Salt key, "
salt-key -y -a $(hostname) 2>&1>/dev/null
if [[ $? -eq 0 ]];then
    echo "accepted."
else
    echo "well this is embarrassing (Salt key not accepted)."
    exit 1
fi

# Add crontab entry.
echo "* * * * * salt '*' saltutil.refresh_pillar" > /var/spool/cron/crontabs/root
service cron start


openssl genrsa -out /etc/ssl/private/key.pem 4096
openssl req -new -x509 -key /etc/ssl/private/key.pem -out /etc/ssl/private/cert.pem -days 1826 -subj "/C=AU/ST=Some-State"
/usr/bin/salt-api